# JavaFX Slideshow

This is a Java 11 JavaFx application to create a JPEG slideshow.


## How to run

At this point, on prototype/alpha stage, proper packaging has not been enabled and only tested on mac. 
Thus, to run the application you require to execute the maven command:

```
mvn clean javafx:run
```

The `JAVA_HOME` property needs to be set on a Java 11.


## The application

To start a slideshow in the applicatio go to the `Menu -> File -> Open`. The application will 
prompt the user to select a directory of his liking, if the directory had jpeg files it will display
them with a delay of a few seconds between them.