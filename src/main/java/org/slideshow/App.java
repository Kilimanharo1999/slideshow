package org.slideshow;

import java.io.IOException;
import java.util.Objects;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        // TODO: Create initial screen that can only select a folder. Drag&Drop + click
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/slideshow.fxml")));
        // TODO: Select start size based on resolution & allow resize when at polish stage since image /vbox does not want to resize
        var scene = new Scene(root, 640, 480);
        stage.setScene(scene);
        stage.setMinHeight(480);
        stage.setMinWidth(640);
        stage.setMaxHeight(480);
        stage.setMaxWidth(640);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}