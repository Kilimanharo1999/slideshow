package org.slideshow;

import javafx.scene.image.Image;

public class Slide {
    private final String title;
    private final int index;
    private final Image image;

    public Slide(String title, int index, Image image) {
        this.title = title;
        this.index = index;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public int getIndex() {
        return index;
    }

    public Image getImage() {
        return image;
    }

}
