package org.slideshow;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;

import javafx.scene.image.Image;

public class SlideShow {

    private static final List<String> JPEG_EXTENSIONS = List.of(".jpg", ".jpeg");

    private final List<Slide> jpegs;
    private int position;

    public SlideShow(File directory) {
        Objects.requireNonNull(directory);
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException(String.format("Path '%s' is not a directory", directory));
        }
        var selectedJpegs = directory.listFiles(file ->
                file.isFile()
                        && JPEG_EXTENSIONS.stream().anyMatch(extension -> file.getName().endsWith(extension))
                        && isItReallyAJpeg(file)
        );
        jpegs = new ArrayList<>();
        if (selectedJpegs != null) {
            Arrays.sort(selectedJpegs, Comparator.comparing(File::getName));
            for (File file : selectedJpegs) {
                try (var inputStream = new FileInputStream(file)) {
                    jpegs.add(new Slide(file.getName(), jpegs.size() + 1, new Image(inputStream)));
                } catch (IOException exception) {
                    // ignore this image
                }
            }
        }
        position = 0;
    }

    public int size() {
        return jpegs.size();
    }

    public boolean isFinished() {
        return position >= jpegs.size();
    }

    public Slide nextSlide() {
        if (isFinished()) {
            throw new NoSuchElementException();
        }
        var slide = jpegs.get(position);
        position++;
        return slide;
    }

    public void reset() {
        position = 0;
    }

    private boolean isItReallyAJpeg(File file) {
        try (var input = ImageIO.createImageInputStream(file)) {
            Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("JPEG");
            while (readers.hasNext()) {
                var reader = readers.next();
                reader.setInput(input);
                try { // TODO: Make it nicer later
                    reader.read(0);
                    return true; // If it can be read, it is a true JPEG.
                } catch (IOException exception) {
                }
            }
        } catch (IOException exception) {
            return false;
        }
        return false;
    }
}
