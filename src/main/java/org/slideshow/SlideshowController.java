package org.slideshow;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.util.Duration;

public class SlideshowController {

    private static final Duration SLIDE_DURATION = Duration.seconds(3);
    private static final String PAUSE_TEXT = "pause";
    private static final String PLAY_TEXT = "play";

    private Timeline slideShowAnimation;
    private SlideShow slideshow;

    @FXML
    MenuBar menuBar;

    @FXML
    MenuItem openDirectoryMenuItem;

    @FXML
    ImageView slideImageView;

    @FXML
    Text slideTitle;

    @FXML
    Text slideCountText;

    @FXML
    Button playButton;

    @FXML
    Button replayButton;

    @FXML
    public void initialize() {
        //menuBar.setUseSystemMenuBar(true); TODO: Set this in a manner that does not get ugly
        playButton.setVisible(false);
        replayButton.setVisible(false);
        // TODO: Add shortcuts/accelerators
    }

    @FXML
    public void openDirectoryAction() {
        var directoryChooser = new DirectoryChooser();
        var selectedDirectory = directoryChooser.showDialog(openDirectoryMenuItem.getParentPopup().getScene().getWindow());
        if (selectedDirectory != null) {
            slideshow = new SlideShow(selectedDirectory);
            initializeAnimation();
        }
    }

    @FXML
    public void closeAction() {
        if (slideShowAnimation != null) {
            slideShowAnimation.stop();
        }
        Platform.exit();
    }

    @FXML
    public void playOrPauseMouseReleased() {
        if (slideShowAnimation != null && !slideshow.isFinished()) {
            if (playButton.getText().equals(PLAY_TEXT)) {
                playButton.setText(PAUSE_TEXT);
                slideShowAnimation.play();
            } else {
                playButton.setText(PLAY_TEXT);
                slideShowAnimation.pause();
            }
        }
    }

    @FXML
    public void replayMouseReleased() {
        slideshow.reset();
        initializeAnimation();
    }

    private void initializeAnimation() {
        if (slideShowAnimation != null) {
            slideShowAnimation.pause();
        }
        slideCountText.setText("0/0");
        slideTitle.setText("");
        slideImageView.setImage(null);
        playButton.setVisible(true);
        playButton.setText(PAUSE_TEXT);
        replayButton.setVisible(true);

        EventHandler<ActionEvent> eventHandler = e -> {
            if (!slideshow.isFinished()) {
                var slide = slideshow.nextSlide();
                slideTitle.setText(slide.getTitle());
                slideCountText.setText(String.format("%d/%d", slide.getIndex(), slideshow.size()));
                slideImageView.setImage(slide.getImage());
                // TODO: Remove when resize enabled
                slideImageView.setPreserveRatio(false);
                slideImageView.setFitHeight(350);
                slideImageView.setFitWidth(623);
            }
        };
        slideShowAnimation = new Timeline(
                new KeyFrame(Duration.ZERO, eventHandler),
                new KeyFrame(SLIDE_DURATION, eventHandler)
        );
        slideShowAnimation.setCycleCount(slideshow.size());
        slideShowAnimation.play();

    }

}
