package org.slideshow;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;

import javafx.stage.Stage;

class SlideshowApplicationTest extends ApplicationTest {

    @BeforeAll
    public static void setUpClass() throws Exception {
        ApplicationTest.launch(App.class);
    }

    @Override
    public void start(Stage stage) {
        stage.show();
    }

    @Test
    void closesTheAppWithTheMenuBar() {
        // A user closes the app with the menubar option
        Assertions.fail("todo");
    }

    @Test
    void selectEmptyDirectory() {
        // A user opens a folder with no jpg images
        Assertions.fail("todo");
    }

    @Test
    void selectSlideshow() {
        // A user opens a folder with jpg images mixed with non jpg images
        // and watches all the slideshow
        Assertions.fail("todo");
    }

    @Test
    void pauseSlideshow() {
        // A user opens a folder with jpg images mixed with non jpg images a
        //and then pauses the execution for a while and resumes it
        Assertions.fail("todo");
    }

    @Test
    void restartsSlideshow() {
        // A user opens a folder with jpg images mixed with non jpg images a
        // watches all the slideshow and then restarts the slideshow
        Assertions.fail("todo");
    }

    @Test
    void selectsAnotherSlideshow() {
        // A user opens a folder with jpg images mixed with non jpg images a
        // realizes he commit a mistake and opens another folder while the
        // slideshow was running
        Assertions.fail("todo");
    }
}
